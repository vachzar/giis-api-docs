

# Authentication

> To authorize, use this code:

```shell
# With shell, you can just pass the correct header with each request
curl "api_endpoint_here" \
  -H "Authorization: Bearer <token API>"
```

```php
use GuzzleHttp\Client;

$client = new Client([
    // Base URI is used with relative requests
    'base_uri' => 'https://giis.jatimprov.go.id/api/',
    'headers' => [
        'Content-Type'     => 'application/json', //Set Content-Type json 
        'Accept'     => 'application/json',
        'Authorization' => 'Bearer [Token API]',
    ]
    // You can set any number of default request options.
    'timeout'  => 2.0,
]);
```

> Pastikan untuk mengubah `[Token API]` dengan Token API anda.

GIIS menggunakan Token API untuk mengizinkan Anda mengakses API. Anda bisa mendapatkan token pada halaman profil [Akun GIIS Anda](https://giis.jatimprov.go.id/admin/user/change_pass).

GIIS mewajibkan Token API untuk disertakan di setiap request API ke server dalam header yang terlihat seperti berikut:

`Authorization: Bearer [Token API]`

<aside class="notice">
Anda harus mengganti <code>[Token API]</code> dengan Token API personal.
</aside>

<aside class="success">
Rekomendasi dan contoh pada dokumentasi ini untuk bahasa Programming <strong>PHP</strong> agar menggunakan <a href="https://docs.guzzlephp.org">Guzzle PHP</a> via Composer, tapi Anda juga bisa menggunakan <a href="https://www.php.net/manual/en/book.curl.php">PHP cURL</a> atau library PHP lainnya.
</aside>
