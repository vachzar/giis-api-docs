# Industri

## Properties Data Industri

Properties ini digunakan untuk tambah dan update data Industri pada endpoint [tambah](#tambah-industri) dan [update](#update-industri).

### Industri properties

| Attribute                     | Type      | Description                                                                                                                          |
| ----------------------------- | --------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| `badan_usaha`                          | string   | Jenis Badan Usaha.   Opsi: `PT`, `PR`, `CV`, `PT Perorangan` dan `TBK`.|
| `nama_perusahaan`                        | string    | Nama Perusahaan. <i class="label label-info">mandatory</i> |
| `nama_pemilik`                      | string    | nama Pemilik usaha.|
| `jalan`                | string | alamat lokasi pabrik / usaha. <i class="label label-info">mandatory</i>|
| `id_kabupaten`            | integer | ID Kota/Kabupaten. Gunakan endpoint [Kota](#kota) untuk mendapatkan ID |
| `id_kecamatan`            | integer | ID Kecamatan. Gunakan endpoint [Kecamatan](#kecamatan) untuk mendapatkan ID|
| `id_kelurahan`            | integer | ID Desa/Kelurahan. Gunakan endpoint [Desa/Kelurahan](#desa) untuk mendapatkan ID|
| `alamat_kantor` | string | alamat kantor jika lokasi berbeda dengan tempat usaha/pabrik. |
| `telepon`               | string    | Nomor Telepon perusahaan |
| `fax`                 | string    | Nomor Fax perusahaan.  |
| `email`                | string    | alamat email perusahaan  |
| `website`            | string    | alamat website perusahaan |
| `tk_laki`                 | integer   | Tenaga Kerja Laki-Laki |
| `tk_perempuan`              | boolean   | Tenaga Kerja Perempuan |
| `nilai_investasi`                 | string     | Nilai investasi perusahaan |
| `nilai_bb`        | string     | Nilai bahan baku per tahun |
| `npwp`                 | string   | Nomor Pokok Wajib Pajak perusahaan |
| `nib`        | string   | Nomor Induk Berusaha |
| `latitude`      | string   | Koordinat latitude |
| `latitude`      | string   | Koordinat latitude |
| `source_data`          | string     | Sumber data. Gunakan `Aplikasi Kab/Kota` jika sinkronisasi data dari aplikasi Kab/Kota <i class="label label-info">mandatory</i>|
| `api_uid` | string     | jika ingin menambahkan ID dari Aplikasi Asal |
| `produk`          | array   | List Data Produk. Cek [Produk Properties](#produk-properties) untuk detail. |

### Produk properties

| Attribute                     | Type      | Description                                                                                                                          |
| ----------------------------- | --------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| `kbli_kode`                          | string   | Kode KBLI Gunakan endpoint [KBLI](#kbli) untuk mendapatkan Kode KBLI|
| `jenis_produk`                        | string    | Nama produk. <i class="label label-info">mandatory</i> |
| `jumlah`                      | string    | Jumlah Kapasitas Produksi per tahun.|
| `satuan`                | string | kode satuan produk. Gunakan endpoint [Satuan](#satuan) untuk mendapatkan Kode Satuan |
| `nilai_produksi`            | string | Nilai Produksi Pertahun |

## Semua Data Industri

Endpoint untuk mendapatkan Semua Industri.

### HTTP Request

<div class="api-endpoint">
	<div class="endpoint-data">
		<i class="label label-get">GET</i>
		<h6>/industri</h6>
	</div>
</div>

> GET https://giis.jatimprov.go.id/api/industri

```shell
curl "https://giis.jatimprov.go.id/api/industri" \
  -H "Authorization: Bearer <token API>"
```

```php
# asumsi sudah deklarasi variable $client dari Guzzle PHP
$client->get('industri');
// Mengirim a GET request to https://giis.jatimprov.go.id/api/industri

#contoh dengan parameter
$client->get('industri?page=1&limit=1000');
// Mengirim a GET request to https://giis.jatimprov.go.id/api/industri?page=1&limit=1000

## contoh dengan parameter dan Request Options GuzzlePHP
$client->get('industri', ['query' => [
    'page' => 1, 
    'id_kabupaten' => 3516
    ]]);
// Mengirim a GET request to https://giis.jatimprov.go.id/api/industri?page=1&id_kabupaten=3516

```

> Perintah di atas akan menghasilkan JSON array seperti ini. **Response 200** :

```json
[
    {
        "id": "188166",
        "badan_usaha": "PT",
        "nama_perusahaan": "PT. USAHA AJA DULU",
        "nama_pemilik": "Budi Baperterus",
        "jalan": "JL. BEBAS HAMBATAN",
        "kelurahan": "MIRIP",
        "kecamatan": "JETIS",
        "kabupaten": "KABUPATEN MOJOKERTO",
        "komoditi": "Makanan",
        "jenis_produk": "PENYEDAP MASAKAN",
        "latitude": "-7.4483621",
        "longitude": "112.4501748",
        "klasifikasi": "MENENGAH",
    },
    {
      ...
    }
]
```

### Query Parameters
parameter tambahan/opsi saat request

contoh: `GET /industri?page=10&limit=1000`

Parameter | Default | Mandatory | Description
--------- | ------- | --------- | -----------
id_kabupaten | null | Opsional (integer) | ID Kabupaten dapatkan dari Endpoint [`/kota`](#kota)
source_data | null | Opsional (string) | Sumber data (Gunakan "Aplikasi Kab/Kota")
page | 1 | Opsional (integer) | Halaman untuk mengakses.
limit | 100 | Opsional (integer) | Batas tampilan data industri dalam sekali request (maksimal 1000).

<aside class="success">
User Admin akan mendapatkan kolom respons lebih banyak.
</aside>

<aside class="notice">
User Kabupaten/Kota karena faktor keamanan secara default respons yang muncul sudah di-filter berdasarkan Kabupaten/Kota masing-masing.
</aside>
## Tambah Industri

Endpoint untuk menambah data industri.

### HTTP Request

<div class="api-endpoint">
	<div class="endpoint-data">
		<i class="label label-post">POST</i>
		<h6>/industri</h6>
	</div>
</div>

> POST https://giis.jatimprov.go.id/api/industri

```php
$industri = [
  'badan_usaha' => "",
  'nama_perusahaan' => "Benkjack",
  'nama_pemilik' => 'Bank',
  'jalan' => 'jalan',
  'id_kabupaten' => '3516',
  'id_kecamatan' => '',
  'id_kelurahan' => '',
  'telepon' => '081',
  'fax' => "",
  'email' => "",
  'website' => "",
  'tk_laki' => '100',
  'tk_perempuan' => '200',
  'nilai_investasi' => 0,
  'nilai_bb' => 0,
  'npwp' => "",
  'nib' => "",
  'latitude' => "",
  'longitude' => "",
  'source_data' => 'Aplikasi Kab/Kota',
  'pemasaran_ekspor' => 'Lokal',
  'produk' => [
      [
          'kbli_kode' => 10734,
          'jenis_produk' => 'Permen',
          'jumlah' => 1000,
          'nilai_produksi' => 10000000,
          'satuan' => 'PCE',
      ],
      [
          'kbli_kode' => 10794,
          'jenis_produk' => 'Kerupuk',
          'jumlah' => 1500,
          'nilai_produksi' => 15000000,
          'satuan' => 'PCE',
      ]
  ],
  'api_uid' => '1010'
];
$client->post('industri', [
  'json' => $industri
]);
```

```shell
curl -X POST "https://giis.jatimprov.go.id/api/industri" \
  -H "Authorization: Bearer <token API>" \ 
  -H "Content-Type: application/json" \
  -d '{
    "badan_usaha": "PR",
    "nama_perusahaan": "PR Dapur Lezat",
    "nama_pemilik": "Dwi",
    "jalan": "Ds besuk rt 09 rw 02, Besuk, Gurah, Kabupaten Kediri, Jawa Timur, 64181",
    "id_kabupaten": "3506",
    "id_kecamatan": "350610",
    "id_kelurahan": "3506102012",
    "telepon": "085211313337",
    "email": "dapurdwifi@gmail.com",
    "nib": "9120119150842",
    "source_data": "Aplikasi Kab/Kota",
    "alamat_kantor": "Dsn Besuk Rt 09 rw 02, Besuk, Gurah, Kabupaten Kediri, Jawa Timur, 64181",
    "produk": [
        {
            "kbli_kode": "10792",
            "jenis_produk": "Kue basah",
            "jumlah": "35",
            "satuan": "NMP",
            "nilai_produksi": "0"
        },
        {
            "kbli_kode": "10710",
            "jenis_produk": "bakeri roti pisang dan lain2",
            "jumlah": "13",
            "satuan": "NMP",
            "nilai_produksi": "0"
        },
    ],
    "nilai_investasi": "100000000",
    "nilai_bb": "50000000",
    'api_uid' : "10100"
}'
```

> **Response 200**

```json
{
  "message": "Industri berhasil ditambahkan",
  "id_industri": 669028
}
```
> **Response 400**

```json
{
  "message": "Industri gagal ditambahkan",
  "errors":  {
    "nama_perusahaan" : "nama_perusahaan is required",
    "jalan" : "jalan is required"
  }
}
```

### JSON Body Request 

Gunakan data `json` dari properties [Industri](#properties-data-industri) untuk dikirim sebagai data bagian body dari request. 

<aside class="notice">
Tidak harus semua properties dimasukkan kedalam request insert. Hanya yang <strong>mandatory</strong> dan yang tersedia saja.
</aside>

<aside class="notice">
User Kabupaten/Kota karena faktor keamanan secara default kolom <code>id_kabupaten</code> otomatis terisi berdasarkan Kabupaten/Kota masing-masing.
</aside>

## Detail Industri
Endpoint untuk mendapatkan detail Industri.

### HTTP Request

<div class="api-endpoint">
	<div class="endpoint-data">
		<i class="label label-get">GET</i>
		<h6>/industri/&lt;ID&gt;</h6>
	</div>
</div>

> GET https://giis.jatimprov.go.id/api/industri/669020

```php
$client->get('industri/669020');
```

```shell
curl "https://giis.jatimprov.go.id/api/industri/669020" \
  -H "Authorization: Bearer <token API>"
```

> **Response 200**

```json
{
    "id": "669020",
    "badan_usaha": "PR",
    "nama_perusahaan": "PR Dapur Lezat Dwifi",
    "nama_pemilik": null,
    "jalan": "Ds besuk rt 09 rw 02, Besuk, Gurah, Kabupaten Kediri, Jawa Timur, 64181",
    "id_kecamatan": "350610",
    "id_kelurahan": "3506102012",
    "kelurahan": "Besuk",
    "kecamatan": "Gurah",
    "id_kabupaten": "3506",
    "kabupaten": "Kabupaten Kediri",
    "telepon": "085211313337",
    "fax": "",
    "email": "dapurdwifi@gmail.com",
    "website": "",
    "ijin_ui": null,
    "tahun_berdiri": null,
    "tahun_ijin": null,
    "komoditi": "Makanan | \nMinuman",
    "tahun_data": null,
    "tk_laki": null,
    "tk_perempuan": null,
    "pemasaran_ekspor": null,
    "negara_tujuan_ekspor": null,
    "npwp": null,
    "nib": "9120119150842",
    "tdp": null,
    "latitude": null,
    "longitude": null,
    "sentra_industri": null,
    "keterangan": null,
    "id_sentra": null,
    "source_data": "siinas",
    "klasifikasi": "KECIL",
    "gambar_depan": null,
    "gambar_belakang": null,
    "verified": "Y",
    "created_at": "2023-11-19 19:26:07",
    "api_uid": null,
    "updated_at": "2023-11-19 19:26:07",
    "contact_person": "Dwi Astutik Pemilik - 085211313337",
    "alamat_kantor": "Dsn Besuk Rt 09 rw 02, Besuk, Gurah, Kabupaten Kediri, Jawa Timur, 64181",
    "siinas_id": "QeEk8s38sfDy7kdSRUVXUM2j_H_w_ie9WCSHbzlDFbo,",
    "terdaftar_siinas": "2023-02-02 14:09:45",
    "skala_usaha_siinas": "Belum Melapor/Mengisi Investasi",
    "produk": [
        {
            "id": "257980",
            "id_hdr": "669020",
            "id_komoditi": "0",
            "komoditi": "",
            "kbli_kode": "",
            "jenis_produk": "Kue basah bakeri roti pisang dan lain2",
            "cabang_industri": "",
            "jumlah": "35",
            "satuan": "NMP",
            "nilai_produksi": "0"
        },
        {
            "id": "257981",
            "id_hdr": "669020",
            "id_komoditi": "0",
            "komoditi": "",
            "kbli_kode": "",
            "jenis_produk": "Kue basah bakeri roti pisang dan lain2",
            "cabang_industri": "",
            "jumlah": "13",
            "satuan": "NMP",
            "nilai_produksi": "0"
        },
        {
            "id": "257982",
            "id_hdr": "669020",
            "id_komoditi": "0",
            "komoditi": "",
            "kbli_kode": "",
            "jenis_produk": "Minuman segar rosella",
            "cabang_industri": "",
            "jumlah": "13",
            "satuan": "BO",
            "nilai_produksi": "0"
        },
        {
            "id": "257983",
            "id_hdr": "669020",
            "id_komoditi": "0",
            "komoditi": "",
            "kbli_kode": "",
            "jenis_produk": "Kue kuping gajah",
            "cabang_industri": "",
            "jumlah": "8",
            "satuan": "NMP",
            "nilai_produksi": "0"
        },
        {
            "id": "257984",
            "id_hdr": "669020",
            "id_komoditi": "0",
            "komoditi": "",
            "kbli_kode": "",
            "jenis_produk": "Minuman rosella celup",
            "cabang_industri": "",
            "jumlah": "13",
            "satuan": "BG",
            "nilai_produksi": "0"
        }
    ],
    "industri_kbli": [
        {
            "id": "1334",
            "kode_kbli": "10710",
            "deskripsi": "INDUSTRI PRODUK ROTI DAN KUE"
        },
        {
            "id": "1335",
            "kode_kbli": "10792",
            "deskripsi": "INDUSTRI KUE BASAH"
        },
        {
            "id": "1336",
            "kode_kbli": "11090",
            "deskripsi": "INDUSTRI MINUMAN LAINNYA"
        },
        {
            "id": "1337",
            "kode_kbli": "10723",
            "deskripsi": "INDUSTRI SIROP"
        }
    ],
    "nilai_investasi": null,
    "total_nilai_produksi": "0",
    "nilai_bahan_baku": null
}
```



### URL Parameters

Parameter | Description
--------- | -----------
ID | ID Industri

## Update Industri

Endpoint untuk memperbarui data industri.

### HTTP Request

<div class="api-endpoint">
	<div class="endpoint-data">
		<i class="label label-post">POST</i>
		<h6>/industri/&lt;ID&gt;</h6>
	</div>
</div>

> POST https://giis.jatimprov.go.id/api/industri/669020

```php
$industri = [
  'badan_usaha' => "PT",
  'nama_perusahaan' => "Benkjack",
  'nama_pemilik' => 'Bang Jaka',
  'jalan' => 'jalan',
  'telepon' => '081',
  'tahun_berdiri' => 0,
  'tahun_ijin' => 0,
  'tahun_data' => 2021,
  'tk_laki' => '100',
  'tk_perempuan' => '200',
  'pemasaran_ekspor' => 'Lokal',
  'produk' => [
      [
          'kbli_kode' => 10734,
          'jenis_produk' => 'Permen',
          'jumlah' => 1000,
          'nilai_produksi' => 10000000,
          'satuan' => 'PCE',
      ],
      [
          'kbli_kode' => 10794,
          'jenis_produk' => 'Kerupuk',
          'jumlah' => 1500,
          'nilai_produksi' => 15000000,
          'satuan' => 'PCE',
      ]
  ],
  'api_uid' => '1010'
];
$client->post('industri', [
  'json' => $industri
]);
```

```shell
curl -X POST "https://giis.jatimprov.go.id/api/industri/669020" \
  -H "Authorization: Bearer <token API>" \ 
  -H "Content-Type: application/json" \
  -d '{
    "badan_usaha": "PR",
    "nama_perusahaan": "PR Dapur Lezat",
    "nama_pemilik": "Dwi",
    "jalan": "Ds besuk rt 09 rw 02, Besuk, Gurah, Kabupaten Kediri, Jawa Timur, 64181",
    "id_kabupaten": "3506",
    "id_kecamatan": "350610",
    "id_kelurahan": "3506102012",
    "telepon": "085211313337",
    "email": "dapurdwifi@gmail.com",
    "nib": "9120119150842",
    "source_data": "Aplikasi Kab/Kota",
    "alamat_kantor": "Dsn Besuk Rt 09 rw 02, Besuk, Gurah, Kabupaten Kediri, Jawa Timur, 64181",
    "produk": [
        {
            "kbli_kode": "10792",
            "jenis_produk": "Kue basah",
            "jumlah": "35",
            "satuan": "NMP",
        },
        {
            "kbli_kode": "10710",
            "jenis_produk": "bakeri roti pisang dan lain2",
            "jumlah": "13",
            "satuan": "NMP",
        },
    ],
    "nilai_investasi": "100000000",
    "nilai_bb": "50000000"
}'
```

> **Response 200**

```json
{
  "message": "Industri berhasil diperbarui",
  "id_industri": 669028
}
```
> **Response 403**

```json
{
  "message": "Data SIINas tidak bisa di-update!",
}
```

> **Response 404**

```json
{
  "message": "Industri Tidak Ditemukan!",
}
```

### URL Parameters

Parameter | Description                                        |
--------- | ---------------------------------------------------|
ID        | ID Industri                                        |

### JSON Body Request 

Gunakan data `json` dari properties [Industri](#properties-data-industri) untuk dikirim sebagai data bagian  body dari request. 

<aside class="notice">
Tidak harus semua properties dimasukkan kedalam request update. Hanya yang berubah saja.
</aside>