
# Introduction

> **base uri** https://giis.jatimprov.go.id/api/

Selamat Datang di GIIS Jatim API v4! Anda bisa menggunakan API kami untuk mengakses GIIS Jatim API endpoints, yang bisa memberikan informasi mengenai data industri, sentra, dan perkembangan industri di lingkungan provinsi Jawa Timur.

API kami bisa diakses dengan beberapa bahasa pemrograman (namun tidak terbatas pada) Shell and PHP! Anda bisa melihat contoh kode di area sebelah kanan, dan bisa mengganti contoh bahasa programmingnya dengan tab di atas kanan.