# Data Master

## KBLI

Endpoint untuk mendapatkan Semua KBLI.

### HTTP Request

<div class="api-endpoint">
	<div class="endpoint-data">
		<i class="label label-get">GET</i>
		<h6>/kbli</h6>
	</div>
</div>

> GET https://giis.jatimprov.go.id/api/kbli

```shell
curl "https://giis.jatimprov.go.id/api/kbli" \
  -H "Authorization: Bearer <token API>"
```

```php
$client->get('kbli');
```
> **Response 200**

```json
[
   {
        "kbli_kode": "10330",
        "deskripsi": "INDUSTRI PENGOLAHAN SARI BUAH DAN SAYURAN",
        "jenis_kbli": "Industri Manufaktur",
        "keterangan": "Kelompok ini mencakup usaha pengawetan buah-buahan dan sayuran dengan cara pengolahan sari buah-buahan dan sayuran, seperti bubuk sari buah-buahan, air/sari pekat buah-buahan dan air/sari pekat sayuran (konsentrat), nektar buah dan atau sayuran."
    },
    {
        "kbli_kode": "10391",
        "deskripsi": "INDUSTRI TEMPE KEDELAI",
        "jenis_kbli": "Industri Manufaktur",
        "keterangan": "Kelompok ini mencakup usaha pembuatan tempe dari kedelai. Usaha pembuatan tempe yang bahan bakunya selain kedelai (dari kacang tanah/kacang-kacangan lainnya), seperti tempe bongkrek, dimasukkan dalam kelompok 10393."
    },
    {
        "kbli_kode": "10392",
        "deskripsi": "INDUSTRI TAHU KEDELAI",
        "jenis_kbli": "Industri Manufaktur",
        "keterangan": "Kelompok ini mencakup usaha pembuatan tahu dari kedelai. Usaha pembuatan tahu yang bahan bakunya selain kedelai (dari kacang tanah/kacang-kacangan lainnya) dimasukkan dalam kelompok 10393."
    },
    {
      ...
    }
]
```
## Satuan

Endpoint untuk mendapatkan Semua Satuan.

### HTTP Request

<div class="api-endpoint">
	<div class="endpoint-data">
		<i class="label label-get">GET</i>
		<h6>/satuan</h6>
	</div>
</div>

> GET https://giis.jatimprov.go.id/api/satuan

```shell
curl "https://giis.jatimprov.go.id/api/satuan" \
  -H "Authorization: Bearer <token API>"
```

```php
$client->get('satuan');
```
> **Response 200**

```json
[
   {
        "id": "287",
        "kd_sat": "TNE",
        "uraian": "ton"
    },
    {
        "id": "288",
        "kd_sat": "LTR",
        "uraian": "liter"
    },
    {
        "id": "289",
        "kd_sat": "KGM",
        "uraian": "kilogram"
    },
    {
        "id": "290",
        "kd_sat": "MTR",
        "uraian": "meter"
    },
    {
      ...
    }
]
```

## Kota

Endpoint untuk mendapatkan Semua Kota dan Kabupaten di Jawa Timur.

### HTTP Request

<div class="api-endpoint">
	<div class="endpoint-data">
		<i class="label label-get">GET</i>
		<h6>/kota</h6>
	</div>
</div>

> GET https://giis.jatimprov.go.id/api/kota

```shell
curl "https://giis.jatimprov.go.id/api/kota" \
  -H "Authorization: Bearer <token API>"
```

```php
$client->get('kota');
```
> **Response 200**

```json
[
   {
        "id": "3501",
        "id_provinsi": "35",
        "kab_kota": "KABUPATEN PACITAN"
    },
    {
        "id": "3502",
        "id_provinsi": "35",
        "kab_kota": "KABUPATEN PONOROGO"
    },
    {
        "id": "3503",
        "id_provinsi": "35",
        "kab_kota": "KABUPATEN TRENGGALEK"
    },
    {
        "id": "3504",
        "id_provinsi": "35",
        "kab_kota": "KABUPATEN TULUNGAGUNG"
    },
    {
        "id": "3505",
        "id_provinsi": "35",
        "kab_kota": "KABUPATEN BLITAR"
    },
    {
      ...
    }
]
```

## Kecamatan

<div class="api-endpoint">
	<div class="endpoint-data">
		<i class="label label-get">GET</i>
		<h6>/kecamatan</h6>
	</div>
</div>

> GET https://giis.jatimprov.go.id/api/kecamatan

```shell
curl "https://giis.jatimprov.go.id/api/kecamatan" \
  -H "Authorization: Bearer <token API>"
```

```php
$client->get('kecamatan');
```
> **Response 200**

```json
[
    {
        "id": "350101",
        "id_kab": "3501",
        "kec": "DONOROJO"
    },
    {
        "id": "350102",
        "id_kab": "3501",
        "kec": "PRINGKUKU"
    },
    {
        "id": "350103",
        "id_kab": "3501",
        "kec": "PUNUNG"
    },
    {
        "id": "350104",
        "id_kab": "3501",
        "kec": "PACITAN"
    },
    {
        "id": "350105",
        "id_kab": "3501",
        "kec": "KEBONAGUNG"
    },
    {
      ...
    }
]
```
### Query Parameters
parameter tambahan/opsi saat request

contoh: `GET /kecamatan?id_kab=3501`

Parameter | Default | Mandatory | Description
--------- | ------- | --------- | -----------
id_kab | null | Opsional (integer) | ID Kabupaten dapatkan dari Endpoint [`/kota`](#kota)

<aside class="notice">
User Kabupaten/Kota karena faktor keamanan secara default respons yang muncul sudah di-filter berdasarkan Kabupaten/Kota masing-masing.
</aside>

## Desa


<div class="api-endpoint">
	<div class="endpoint-data">
		<i class="label label-get">GET</i>
		<h6>/desa</h6>
	</div>
</div>

> GET https://giis.jatimprov.go.id/api/desa

```shell
curl "https://giis.jatimprov.go.id/api/desa" \
  -H "Authorization: Bearer <token API>"
```

```php
$client->get('desa');
```
> **Response 200**

```json
[
     {
        "id": "3501012001",
        "id_kec": "350101",
        "kel": "WIDORO"
    },
    {
        "id": "3501012002",
        "id_kec": "350101",
        "kel": "SAWAHAN"
    },
    {
        "id": "3501012003",
        "id_kec": "350101",
        "kel": "KALAK"
    },
    {
        "id": "3501012004",
        "id_kec": "350101",
        "kel": "SENDANG"
    },
    {
      ...
    }
]
```
### Query Parameters
parameter tambahan/opsi saat request

contoh: `GET /desa?id_kec=350101`

Parameter | Default | Description
--------- | --------| -----------
id_kec | null | ID Kabupaten dapatkan dari Endpoint [`/kecamatan`](#kecamatan) <i class="label label-info">Opsional</i> 

<aside class="notice">
User Kabupaten/Kota karena faktor keamanan secara default respons yang muncul sudah di-filter berdasarkan Kabupaten/Kota masing-masing.
</aside>