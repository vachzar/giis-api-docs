# Responses Code

<aside class="notice">
GIIS Jatim API sangat bergantung dengan kode respons untuk memberitahu pengguna tentang status request yang dipanggil. Jadi pastikan untuk mengecek status setiap request yang dipanggil
</aside>

GIIS Jatim API menggunakan kode respons sebagai berikut:


HTTP Code   | Deskripsi
----------: | ---------
200         | OK -- Request Anda Valid 
400         | Bad Request -- ada kesalahan pada request. Cek respons `message` dan `errors`.
401         | Unauthorized -- Token API key salah.
403         | Forbidden -- Data atau Endpoint hanya untuk user Admin. cek respons `message` untuk detail.
404         | Not Found -- Data tidak ditemukan.