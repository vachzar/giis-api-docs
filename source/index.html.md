---
title: GIIS JATIM API V4 Reference

language_tabs: # must be one of https://github.com/rouge-ruby/rouge/wiki/List-of-supported-languages-and-lexers
  - php : Guzzle PHP
  - shell

toc_footers:
  - GIIS API v4.0 compiled by JAR
  - <a href='https://giis.jatimprov.go.id/admin/user/change_pass'>Get Token API GIIS</a>
  - <a href='https://github.com/slatedocs/slate'>Powered by Slate</a>

includes:
  - introduction
  - authentication
  - responses
  - industri
  - master

search: true

code_clipboard: true

meta:
  - name: description
    content: Dokumentasi GIIS JATIM API V4
---